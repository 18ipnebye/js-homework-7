"use strict"

// 1
let isPalindrome = function(str){
    let strCheke = '';
    for(let i = str.length-1; i >= 0; i--){
        strCheke += str[i]
    }
    let compare = str.localeCompare(strCheke)
   
    return compare === 0
   
}
console.log(isPalindrome("qwwwwq"))

// 2
let checkLength = function(str, number){
    let strL = str.length;
    let compare = strL <= number;
    return compare
   
}
console.log(checkLength("ffffddsa", 1));

// 3
let userBirth = function(){
    let formatDate = /^\d{4}\/(0[1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])$/;
    let getUserBirthday = null;
    while (!getUserBirthday || !formatDate.test(getUserBirthday)){
        getUserBirthday = prompt("what year your birth", "yyyy/mm/dd");
    }
   
    let nowDate  = new Date();
    let userOld = new Date(getUserBirthday);
    let countAge = Math.floor((nowDate - userOld)/1000/60/60/24/365);

    if(countAge<=0){
        alert(`you wrot date before your birth, enter date again`);
        return;
    }else if(countAge>100 && countAge<150){
        alert(`you are ${countAge} years old, not many people can live to this age`);
        return;
    }else if(countAge>150){
        alert("wrong enter date");
        return;
    }

    return alert(`you are ${countAge} years old`)

}
userBirth();

